import os
from mycroft import MycroftSkill, intent_file_handler
from youtube_search import YoutubeSearch

from mycroft.audio.services.vlc import VlcService
from mycroft.audio import wait_while_speaking

base_url = 'https://www.youtube.com'


class Youtuber(MycroftSkill):
    def __init__(self):
        MycroftSkill.__init__(self)
        self.mediaplayer = VlcService(config={'low_volume': 10, 'duck': True})
        self.audio_state = 'stopped'
        self.last_video = ''

    @intent_file_handler('youtuber.intent')
    def handle_youtuber_play(self, message):
        music = message.data.get('music')
        results = YoutubeSearch(music, max_results=10).to_dict()

        if (len(results) > 0):
            tracklist = []
            print('results')
            print(results[0])
            url = base_url + results[0]['url_suffix']

            if self.audio_state == "playing":
                self.mediaplayer.stop()
                self.mediaplayer.clear_list()

            tracklist.append(url)
            self.last_video = url
            self.mediaplayer.add_list(tracklist)
            self.speak_dialog('youtuber', data={
                'music': results[0]['title'],
            })
            wait_while_speaking()
            self.audio_state = 'playing'
            self.mediaplayer.play()

        else:
            self.speak_dialog('youtuber.notfound', data={
                'music': music,
            })

    @intent_file_handler('youtuber.show.intent')
    def handle_youtuber_show(self, message):
        video = message.data.get('video')
        results = YoutubeSearch(video, max_results=10).to_dict()

        if (len(results) > 0):
            tracklist = []
            print('results')
            print(results[0])
            url = base_url + results[0]['url_suffix']

            tracklist.append(url)
            self.speak_dialog('youtuber', data={
                'music': results[0]['title'],
            })
            os.system('vlc ' + tracklist[0])

            wait_while_speaking()

        else:
            self.speak_dialog('youtuber.notfound', data={
                'music': video,
            })

    @intent_file_handler('youtuber.next.intent')
    def handle_youtuber_next(self, message):
        music = message.data.get('music')
        results = YoutubeSearch(music, max_results=10).to_dict()

        if (len(results) > 0):
            tracklist = []
            print('results')
            print(results[0])
            url = base_url + results[0]['url_suffix']

            if self.audio_state == "playing":
                self.mediaplayer.stop()
                self.mediaplayer.clear_list()

            tracklist.append(url)
            self.mediaplayer.add_to_list(tracklist)
            self.speak_dialog('youtuber', data={
                'music': results[0]['title'],
            })
            wait_while_speaking()
            self.audio_state = 'playing'
            self.mediaplayer.play()

        else:
            self.speak_dialog('youtuber.notfound', data={
                'music': music,
            })

    @intent_file_handler('youtuber.pause.intent')
    def pause(self, message=None):
        print('pausem usic')
        self.mediaplayer.stop()
        self.mediaplayer.clear_list()

    def shutdown(self):
        if self.audio_state == 'playing':
            self.mediaplayer.stop()
            self.mediaplayer.clear_list()


def create_skill():
    return Youtuber()
